(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   (quote
    (lsp-ui lsp-treemacs treemacs-magit treemacs-icons-dired treemacs-projectile treemacs-evil treemacs company-box company-lsp company lsp-mode rjsx-mode web-mode tide evil-leader evil-nerd-commenter powerline rainbow-delimiters editorconfig which-key spacemacs-theme exec-path-from-shell magit helm-projectile helm general evil use-package))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
